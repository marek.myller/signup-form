'use strict';
//form stuff
const firstName = document.querySelector('.first-name');
const lastName = document.querySelector('.last-name');
const email = document.querySelector('.email');
//const emailPlaceholder = document.querySelector(".email")
const password = document.querySelector('.password');
const btn = document.querySelector('.btn');

// errors
const firstNameError = document.querySelector('.f-n-error');
const lastNameError = document.querySelector('.l-n-error');
const emailError = document.querySelector('.email-error');
const passwordError = document.querySelector('.password-error');

// Initial
const init = function () {
  firstName.classList.remove('empty');
  lastName.classList.remove('empty');
  email.classList.remove('empty');
  email.classList.remove('email-placeholder');
  password.classList.remove('empty');
  ////
  firstNameError.classList.add('hidden');
  lastNameError.classList.add('hidden');
  emailError.classList.add('hidden');
  passwordError.classList.add('hidden');
};
init();

btn.addEventListener('click', function () {
  if (firstName.value === '') {
    firstName.classList.add('empty');
    firstNameError.classList.remove('hidden');
  }
  if (lastName.value === '') {
    lastName.classList.add('empty');
    lastNameError.classList.remove('hidden');
  }
  let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (email.value === '' || !email.value.match(mailformat)) {
    email.classList.add('empty');
    email.classList.add('email-placeholder');
    email.placeholder = 'email@example.com';
    emailError.classList.remove('hidden');
  }
  if (password.value === '') {
    password.classList.add('empty');
    passwordError.classList.remove('hidden');
  }
  if (email.value.match(mailformat) && password.value != '') {
    alert('Looks like it works');
  }
});

firstName.addEventListener('click', function () {
  firstName.classList.remove('empty');
  firstNameError.classList.add('hidden');
});

lastName.addEventListener('click', function () {
  lastName.classList.remove('empty');
  lastNameError.classList.add('hidden');
});

email.addEventListener('click', function () {
  email.classList.remove('empty');
  email.classList.remove('email-placeholder');
  email.placeholder = 'Email Address';
  emailError.classList.add('hidden');
});
password.addEventListener('click', function () {
  password.classList.remove('empty');
  passwordError.classList.add('hidden');
});
